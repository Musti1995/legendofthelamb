package be.vdab.lamb;

import be.vdab.lamb.map.Direction;

import java.util.HashMap;

public class Commands {

    private static final HashMap<String, Command> commandMap = new HashMap<>();

    static {
        commandMap.put("quit", (session, args) -> session.close());
        commandMap.put("go", (session, args) -> {
            if (args.length < 2) {
                System.out.println("Use as 'go <direction>' where direction = (n)orth, (e)ast, (s)outh, (w)est");
                return;
            }
            switch (args[1]) {
                case "north":
                case "n":
                    session.attemptMovement(Direction.NORTH); break;
                case "east":
                case "e":
                    session.attemptMovement(Direction.EAST); break;
                case "south":
                case "s":
                    session.attemptMovement(Direction.SOUTH); break;
                case "west":
                case "w":
                    session.attemptMovement(Direction.WEST); break;
            }
        });
        commandMap.put("routes", (session, args) -> session.showRoutes());
        commandMap.put("map", (session, args) -> session.showMap());
        commandMap.put("help", (session, args) -> {
            System.out.println("List of commands: ");
            System.out.printf("%10s - Return to main menu without saving.\n", "quit");
            System.out.printf("%10s - Try moving in the specified direction.\n", "go");
            System.out.printf("%10s - Shows the different directions you can go from the current room.\n", "routes");
            System.out.printf("%10s - Displays the map with all the rooms you've visited so far.\n", "map");
            System.out.printf("%10s - Saves your progress if you're in a room with a bonfire.\n", "save");
        });
        commandMap.put("save", (session, args) -> session.save(false));
    }

    public static void processInput(Session s) {
        String commandLine = UserIO.readLine().toLowerCase();
        String[] splitCommandLine = commandLine.split(" ");
        Command cmd = commandMap.get(splitCommandLine[0]);
        if (cmd != null) cmd.trigger(s, splitCommandLine);
    }

}

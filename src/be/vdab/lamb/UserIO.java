package be.vdab.lamb;

import java.util.Scanner;

public class UserIO {

    private static final Scanner in = new Scanner(System.in);

    public static void listOptions(String... options) {
        for (int i = 0; i < options.length; i++)
            System.out.println("\t"+(i+1)+".\t"+options[i]);
    }

    public static void listOptionsWithCancel(String... options) {
        int i = 0;
        while (i < options.length) {
            System.out.println("\t" + (i + 1) + ".\t" + options[i]);
            i++;
        }
        System.out.println("\t" + (i + 1) + ".\tCancel");
    }

    public static int pickInRange(int min, int max, String question) {
        while (true) {
            try {
                System.out.print(question);
                int val = Integer.parseInt(in.next());
                if (val >= min && val <= max) {
                    return val;
                } else {
                    System.out.println("Pick a number from range " + min + " to " + max);
                }
            } catch(NumberFormatException nfe) {
                System.out.println("Pick a number from range " + min + " to " + max);
            }
        }
    }

    public static String pickFromOptions(String... options) {
        listOptions(options);
        int choice = pickInRange(1, options.length, "Select: ")-1;
        return options[choice];
    }

    public static String pickFromOptionsCancel(String... options) {
        listOptionsWithCancel(options);
        int choice = pickInRange(1, options.length + 1, "Select: ") -1;
        if (choice == options.length) return "Cancel";
        return options[choice];
    }

    public static String readLine() {
        String input = null;
        while (input == null) {
            input = in.nextLine();
        }
        return input;
    }

    public static String read() {
        String input = null;
        while (input == null) {
            input = in.next();
        }
        return input;
    }

}
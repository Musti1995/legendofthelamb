package be.vdab.lamb.items.weapons;

import be.vdab.lamb.items.Item;

public enum Weapon implements Item{
    SWORD("sword",20, 10, "A sharpened piece of steel, deadly however you look at it"),
    BOW("bow",20, 12, "A piece of wood, stringed with flaxen string. Nothing special, but lethal from a distance."),
    HOLY_SYMBOL("holy symbol",20,6,"an emblem of a God, imbued with its power. Can be used to cast various spells that need support from the Wisdom abilty"),
    DAGGER("dagger", 20,4,"A small knife that can be used in  different situations"),
    CLUB("club", 20, 14, "a heavy piece of wood with spikes at the top"),
    WARHAMMER("warhammer",16, 20, "a very heavy weapon that deos massive damage"),
    CROSSBOW("crossbow", 16,  22, "slow to reload but will pierce armor with ease" ),
    HOLY_SCEPTR("holy sceptr", 20, 8, "a staff imbued with the power of the gods");


    int atk;
    int dmg;
    String info;
    String name;

    Weapon(String name, int atk, int dmg, String info) {
        this.name = name;
        this.atk = atk;
        this.dmg = dmg;
        this.info =info;
    }

    public int getAtk() {
        return atk;
    }

    public void setAtk(int atk) {
        this.atk = atk;
    }

    public int getDmg() {
        return dmg;
    }

    public void setDmg(int dmg) {
        this.dmg = dmg;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package be.vdab.lamb.items.backpacks;

import be.vdab.lamb.items.Item;

import java.util.ArrayList;
import java.util.List;

public enum Packs implements Item {
    BACKPACK(20, 0,500),
    CLERICAL_BACKPACK(15,5,300);

    int normalCap;
    int spellbookCap;
    int maxGold;

    Packs(int normalCap, int spellbookCap, int maxGold){
        this.normalCap = normalCap;
        this.spellbookCap = spellbookCap;
        this.maxGold = maxGold;
    }


}


package be.vdab.lamb.items.armors;

import be.vdab.lamb.items.Item;

public enum Armor implements Item {
    CHAINMAIL("Chain mail", "Many small rings make one very strong jacket", "Will make noise whilst sneaking!", 10),
    LEATHER_ARMOR("Leather armor", "A rough animal hide , tanned and tailored to the user","", 8),
    CLOTHES("clothes", "a woolen tunic", "", 6);

    int armorBase;
    private String name;
    private String info;
    private String warning;

    Armor(String name, String info, String warning ,int armorBase){
        this.name = name;
        this.info = info;
        this.armorBase = armorBase;
        this.warning = warning;
    }

    public int getArmorBase() {
        return armorBase;
    }

    public void setArmorBase(int armorBase) {
        this.armorBase = armorBase;
    }
}



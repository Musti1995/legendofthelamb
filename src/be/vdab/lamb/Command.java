package be.vdab.lamb;

public interface Command {

    void trigger(Session s, String[] args);

}

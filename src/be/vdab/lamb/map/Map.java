package be.vdab.lamb.map;

import be.vdab.lamb.character.Enemy;

import java.io.*;

/*
 * https://www.youtube.com/watch?v=mDlpgIJmwC8
 */
public final class Map implements Serializable {

    private int startingX;
    private int startingY;
    private String name;
    private Room[][] rooms;

    protected Map(String name, int width, int height) {
        this.name = name;
        this.rooms = new Room[height][width];
    }

    private Map() {

    }

    public String getMapName() {
        return name;
    }

    public Room getRoom(int x, int y) {
        try {
            return rooms[y][x];
        } catch(IndexOutOfBoundsException ex) {
            return null;
        }
    }

    public Room getRoom(int x, int y, Direction direction) {
        return getRoom(x + direction.getOffsetX(), y + direction.getOffsetY());
    }

    public void printMap(int currX, int currY) {
        final String HIDDEN = "  ";
        final String VISITED = "x ";
        final String CURRENT = "o ";
        System.out.println();
        for (int y = 0; y < rooms.length; y++) {
            System.out.print("\t");
            for (int x = 0; x < rooms[y].length; x++) {
                if (x == currX && y == currY) {
                    System.out.print(CURRENT);
                    continue;
                }
                Room room = rooms[y][x];
                if (room != null) {
                    if (room.isVisited()) {
                        System.out.print(VISITED);
                        continue;
                    }
                }
                System.out.print(HIDDEN);
            }
            System.out.println();
        }
        System.out.println();
    }

    public void showPossibleRoutes(int x, int y) {
        System.out.println("Possible routes in this room: ");
        Room current = getRoom(x, y);
        for (Direction dir : Direction.values()) {
            Room toCheck = getRoom(x, y, dir);
            if (toCheck != null) {
                System.out.print("\t" + dir.getName());
                Door directionalDoor = current.getDoor(dir);
                if (directionalDoor != null) {
                    if (!directionalDoor.isOpened())
                        System.out.print(": Closed door");
                }
                System.out.print("\n");
            }
        }
    }

    public int getStartingX() {
        return startingX;
    }

    public int getStartingY() {
        return startingY;
    }

    public void placeDoor(int x, int y, Direction dir, Door door) {
        Room toPlace = getRoom(x, y);
        if (toPlace == null) {
            System.out.println("Can't place door at coordinate (" + x + ", " + y + "); no room there.");
            return;
        }
        Room leadsTo = getRoom(x, y, dir);
        if (leadsTo == null) {
            System.out.println("Can't place door at coordinate (" + x + ", " + y + "); leads to nowhere.");
            return;
        }
        toPlace.placeDoor(dir, door);
        leadsTo.placeDoor(dir.getOpposite(), door);
    }

    public static void constructForestMapLayout() {
        Map forest = new Map();
        forest.name = "Introduction Stage";
        forest.rooms = new Room[7][7];
        int[][] mapData = {
                {0, 0, 0, 1, 0, 0, 0},
                {0, 1, 1, 1, 1, 1 ,0},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 0, 1, 0, 0, 0}
        };
        forest.startingX = 3;
        forest.startingY = 6;
        for (int i = 0; i < mapData.length; i++) {
            for (int j = 0; j < mapData[i].length; j++) {
                if (mapData[i][j] == 1) forest.rooms[i][j] = new Room();
            }
        }
        forest.getRoom(3, 5).addBonfire();
        forest.placeDoor(3, 5, Direction.WEST, new OnewayDoor(Direction.WEST));
        forest.rooms[5][4].getEnemies().add(Enemy.createEnemy());
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(".\\maps\\"+forest.name+".ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(forest);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

}

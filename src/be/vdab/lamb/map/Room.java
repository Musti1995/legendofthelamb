package be.vdab.lamb.map;

import be.vdab.lamb.character.Enemy;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class Room implements Serializable {

    private Door[] doors;
    private boolean visited;
    private boolean hasBonfire;
    private List<Enemy> enemies;

    protected Room() {
        this.enemies = new LinkedList<>();
        this.doors = new Door[4];
        this.visited = false;
        this.hasBonfire = false;
    }

    public List<Enemy> getEnemies() {
        return enemies;
    }

    public Door getDoor(Direction dir) {
        return doors[dir.getValue()];
    }

    public void placeDoor(Direction dir, Door door) {
        doors[dir.getValue()] = door;
    }

    public boolean hasBonfire() {
        return hasBonfire;
    }

    public void addBonfire() {
        this.hasBonfire = true;
    }

    public boolean isVisited() {
        return visited;
    }

    public void markVisited() {
        this.visited = true;
    }

}

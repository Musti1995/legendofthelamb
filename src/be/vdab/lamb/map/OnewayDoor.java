package be.vdab.lamb.map;

public class OnewayDoor extends Door {

    private final Direction opensFrom;

    public OnewayDoor(Direction opensFrom) {
        this.opensFrom = opensFrom;
    }

    @Override
    public void open(Direction directionOpenedFrom) {
        if (directionOpenedFrom.getValue() == opensFrom.getValue()) {
            markOpened();
            System.out.println("You unlock the door.");
        } else {
            System.out.println("This door opens from the other side.");
        }
    }
}

package be.vdab.lamb.map;

public enum Direction {

    NORTH(0, 0, -1, "North"),
    EAST(1, 1, 0, "East"),
    SOUTH(2, 0, 1, "South"),
    WEST(3, -1, 0, "West");

    private int value;
    private int offsetX;
    private int offsetY;
    private String name;

    Direction(int value, int offsetX, int offsetY, String name) {
        this.value = value;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public int getOffsetX() {
        return offsetX;
    }

    public int getOffsetY() {
        return offsetY;
    }

    public Direction getOpposite() {
        if (value < 2) return getDirectionForValue(value + 2);
        return getDirectionForValue(value - 2);
    }

    public static Direction getDirectionForValue(int value) {
        if (value == 0) return NORTH;
        if (value == 1) return EAST;
        if (value == 2) return SOUTH;
        if (value == 3) return WEST;
        throw new IllegalArgumentException("Invalid directional value: "+value);
    }

    public boolean isVertical() {
        return value == 0 || value == 2;
    }

    public boolean isHorizontal() {
        return value == 1 || value == 3;
    }

}

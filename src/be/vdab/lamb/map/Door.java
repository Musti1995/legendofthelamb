package be.vdab.lamb.map;

import java.io.Serializable;

public abstract class Door implements Serializable {

    private boolean opened;

    public boolean isOpened() {
        return opened;
    }

    public void markOpened() {
        this.opened = true;
    }

    public abstract void open(Direction directionOpenedFrom);
}

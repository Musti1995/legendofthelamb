package be.vdab.lamb;

import be.vdab.lamb.character.Player;
import be.vdab.lamb.character.PlayerClass;
import be.vdab.lamb.character.PlayerRace;
import be.vdab.lamb.map.Map;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) {
        Map.constructForestMapLayout();
        System.out.println("++++++++++++++++++++++++++++++");
        System.out.println("++  Legend of the Lamb - DA ++");
        System.out.println("++++++++++++++++++++++++++++++");
        System.out.println("--    DA Studios Belgium    --");
        System.out.println("------------------------------");
        boolean running = true;
        while (running) {
            System.out.println("MAIN MENU");
            String[] options = {"New game", "Load game", "Delete game", "Info", "Quit"};
            UserIO.listOptions(options);
            int choice = UserIO.pickInRange(1, options.length, "Select: ") - 1;
            switch (choice) {
                case 0: openNewGameDialogue(); break;
                case 1: openLoadGameDialogue(); break;
                case 2: //delete game:
                    break;
                case 3: //info
                    break;
                case 4: //quit:
                    running = false;
                    break;
            }
        }
    }

    private static void openNewGameDialogue() {
        System.out.print("Enter a name for your character: ");
        String username = UserIO.read();
        System.out.println("pick your characters race: ");
        String[] raceOptions = {"Human", "Elf", "Dwarf"};
        String selectedRace = UserIO.pickFromOptions(raceOptions);
        PlayerRace race;
        switch (selectedRace){
            case "Elf" : race = PlayerRace.ELF;break;
            case "Dwarf": race = PlayerRace.DWARF;break;
            default: race = PlayerRace.HUMAN;
        }
        System.out.println("Pick a class: ");
        String[] classOptions = {"Figther", "Ranger", "Healer"};
        String selectedClass = UserIO.pickFromOptions(classOptions);
        PlayerClass playerClass;
        switch (selectedClass){
            case"Ranger": playerClass = PlayerClass.RANGER;break;
            case"Healer": playerClass = PlayerClass.HEALER; break;
            default: playerClass = PlayerClass.FIGHTER;
        }
        Player player = new Player(username, race, playerClass);


        if (Files.exists(Paths.get(".\\saves\\" + username + ".ser"))) {
            System.out.println("A save game already exists for a character with that name.");
            System.out.println("You can load the save game with option (2) from the main menu " +
                    "or start a new game with a different name.");
            return;
        }
        Map map = null;
        try {
            String[] fileNames = Files.list(Paths.get(".\\maps\\"))
                    .map(Path::toString)
                    .filter((s) -> s.endsWith(".ser"))
                    .toArray(String[]::new);
            if (fileNames.length == 0) {
                System.out.println("Couldn't find any maps to load..");
                return;
            }
            System.out.println("Which map would you like to play on?");
            String selectedMap = UserIO.pickFromOptions(fileNames);
            FileInputStream fileInputStream = new FileInputStream(selectedMap);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            map = (Map)objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        } catch(IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        if (map == null) {
            System.out.println("Failed to find a map to load.");
            return;
        }
        Session session = new Session(player, map);
        session.start();
    }

    private static void openLoadGameDialogue() {
        Session session = null;
        try {
            String[] fileNames = Files.list(Paths.get(".\\saves\\"))
                    .map(Path::toString)
                    .filter((s) -> s.endsWith(".ser"))
                    .toArray(String[]::new);
            if (fileNames.length == 0) {
                System.out.println("No save files detected; please start a new game.");
                return;
            }
            System.out.println("Which save game would you like to load?");
            String selectedSave = UserIO.pickFromOptionsCancel(fileNames);
            if (selectedSave.equals("Cancel")) return;
            FileInputStream fileInputStream = new FileInputStream(selectedSave);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            session = (Session)objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        } catch(IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        if (session == null) {
            System.out.println("Failed to load save game data.");
            return;
        }
        session.start();
    }

}
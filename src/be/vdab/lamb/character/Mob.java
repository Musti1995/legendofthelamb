package be.vdab.lamb.character;

import be.vdab.lamb.Dice;
import be.vdab.lamb.items.weapons.Weapon;

import java.io.Serializable;

public abstract class Mob{
    public abstract Weapon getCurrentWeapon();

    public abstract int getLvl();

    public abstract int getStrMod();

    public abstract int getAc();



    public abstract int getHp();

    public abstract void setHp(int hp);

    public abstract int getDexMod();

    public abstract int getWisMod();

    public abstract double getXp();

    public abstract int getCr();

    public abstract void setXp(double xp);

    public abstract double getXpGain();



}



package be.vdab.lamb.character;

public enum EnemyClass {
    WARRIOR(1,10,8),
    SHAMAN(1, 5, 8),
    BEAST(1, 5, 12),
    WEENIE(0,0,0),
    BOSS(2, 20, 25);

    int cr;
    int ac;
    int hp;

    EnemyClass(int cr, int ac , int hp) {
        this.cr = cr;
        this.ac = ac;
        this.hp = hp;
    }
}

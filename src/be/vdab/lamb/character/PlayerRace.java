package be.vdab.lamb.character;

public enum PlayerRace {
    HUMAN(1,1,1,1,1,1,6 ),
    DWARF(2,0,0,0,0,2,5),
    ELF(0,2,0,2,0,0,7);

    int str;
    int dex;
    int wis;
    int intell;
    int charisma;
    int constit;
    int speed;

    PlayerRace(int str, int dex , int wis , int intell , int charisma, int constit, int speed) {
        this.str =str;
        this.dex =dex;
        this.wis =wis;
        this.intell =intell;
        this.charisma =charisma;
        this.constit =constit;

    }

}
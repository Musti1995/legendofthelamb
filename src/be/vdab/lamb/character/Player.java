package be.vdab.lamb.character;

import be.vdab.lamb.items.Item;
import be.vdab.lamb.items.armors.Armor;
import be.vdab.lamb.items.backpacks.Packs;
import be.vdab.lamb.items.weapons.Weapon;

import java.io.Serializable;
import java.util.List;

public class Player extends Mob implements Serializable {
    private int lvl;
    private double xp;
    Weapon currentWeapon;
    private Armor currentArmor;
    private PlayerClass playerClass;
    private PlayerRace playerRace;
    private int classBaseArmor;
    private int str ;
    private int dex ;
    private int wis ;
    private int intell ;
    private int charisma ;
    private int constit;
    private int lifedice ;
    private int raceBaseSpeed;
    private List<Item> inventory;
    private int hitpoints;
    private int strMod;
    private int dexMod;
    private int wisMod;
    private int constitMod;

    private int ac ;
    private int speed ;
    private String name;





    public  Player(String name, PlayerRace playerRace, PlayerClass playerClass){
        lvl = 1;
        str = playerClass.str + playerRace.str;
        dex = playerClass.dex + playerRace.dex;
        wis = playerClass.wis + playerRace.wis;
        intell = playerClass.intell + playerRace.intell;
        charisma = playerClass.charisma + playerRace.charisma;
        constit = playerClass.constit + playerRace.constit;
        raceBaseSpeed = playerRace.speed;
        lifedice = playerClass.lifedice;
        classBaseArmor = playerClass.classBaseArmor;
        this.name = name;



    }

    public int getHitpoints() {
        return(getLifedice()*3+10)+((getLvl()-1)*((getLifedice()*2)));
    }

    public int getAc() {
        return (getClassBaseArmor()+getCurrentArmor().getArmorBase()+((getStr()-10)/2)) ;
    }

    @Override
    public int getHp() {
        return 0;
    }

    @Override
    public void setHp(int i) {

    }

    public String getName() {
        return name;
    }

    public void setAc(int ac) {
        this.ac = ac;
    }

    public int getSpeed() {
        if((getDex()-10)/2 > (getConstit()-10)/2){
            return(getRaceBaseSpeed()+(getDex()-10)/2 );}
        else {return getRaceBaseSpeed()+(getConstit()-10)/2;}


    }


    @Override
    public double getXpGain() {
        return 0;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getStrMod() {
        return (getStr()-10)/2;
    }

    public void setStrMod(int strMod) {
        this.strMod = strMod;
    }

    public int getDexMod() {
        return (getDex()-10)/2;
    }

    public void setDexMod(int dexMod) {
        this.dexMod = dexMod;
    }

    public int getWisMod() {
        return (getWis()-10/2);
    }

    public void setWisMod(int wisMod) {
        this.wisMod = wisMod;
    }

    public void setItems(PlayerClass playerClass){
        if(playerClass == PlayerClass.FIGHTER){
            setCurrentWeapon(Weapon.SWORD);
            setCurrentArmor(Armor.CHAINMAIL);
            inventory.add(Packs.BACKPACK);



        }
        else if (playerClass == PlayerClass.RANGER){
            setCurrentWeapon(Weapon.BOW);
            inventory.add(Weapon.SWORD);
            setCurrentArmor(Armor.LEATHER_ARMOR);
            inventory.add(Packs.BACKPACK);

        }
        else{
            setCurrentWeapon(Weapon.HOLY_SYMBOL);
            inventory.add(Weapon.DAGGER);
            setCurrentArmor(Armor.CLOTHES);
            inventory.add(Packs.CLERICAL_BACKPACK);
        }


    }
    public void charLvl(){
        if (xp >= 100 && xp < 250){lvl = 2;}
        else if (xp >= 250 && xp <700){lvl = 3;}
        else if (xp >= 700 && xp <1500){lvl = 4;}
        else {lvl = 5;}

    }

    public void rest(){
        if (playerClass.constit + playerRace.constit < playerClass.dex + playerRace.dex){
            setSpeed(playerRace.speed + dexMod);
        }else{
            setSpeed(playerRace.speed + constitMod);
        }
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public double getXp() {
        return xp;
    }

    public void setXp(double xp) {
        this.xp = xp;
    }

    public Weapon getCurrentWeapon() {
        return currentWeapon;
    }

    public void setCurrentWeapon(Weapon currentWeapon) {
        this.currentWeapon = currentWeapon;
    }

    public PlayerClass getPlayerClass() {
        return playerClass;
    }

    public void setPlayerClass(PlayerClass playerClass) {
        this.playerClass = playerClass;
    }

    public PlayerRace getRace() {
        return playerRace;
    }

    public void setRace(PlayerRace playerRace) {
        this.playerRace = playerRace;
    }

    public int getClassBaseArmor() {
        return classBaseArmor;
    }

    public void setClassBaseArmor(int classBaseArmor) {
        this.classBaseArmor = classBaseArmor;
    }

    public int getStr() {
        return str;
    }

    public void setStr(int str) {
        this.str = str;
    }

    public int getDex() {
        return dex;
    }

    public int getConstitMod() {
        return (getConstit()-10)/2;
    }

    public void setConstitMod(int constitMod) {
        this.constitMod = constitMod;
    }

    public void setDex(int dex) {
        this.dex = dex;
    }

    public int getWis() {
        return wis;
    }

    public void setWis(int wis) {
        this.wis = wis;
    }

    public int getIntell() {
        return intell;
    }

    public void setIntell(int intell) {
        this.intell = intell;
    }

    public int getCharisma() {
        return charisma;
    }

    public void setCharisma(int charisma) {
        this.charisma = charisma;
    }

    public int getConstit() {
        return constit;
    }

    public void setConstit(int constit) {
        this.constit = constit;
    }

    public int getLifedice() {
        return lifedice;
    }

    public void setLifedice(int lifedice) {
        this.lifedice = lifedice;
    }

    public int getRaceBaseSpeed() {
        return raceBaseSpeed;
    }

    public void setRaceBaseSpeed(int raceBaseSpeed) {
        this.raceBaseSpeed = raceBaseSpeed;
    }

    public Armor getCurrentArmor() {
        return currentArmor;
    }

    public void setCurrentArmor(Armor currentArmor) {
        this.currentArmor = currentArmor;
    }

    @Override
    public int getCr() {
        return 0;
    }
}

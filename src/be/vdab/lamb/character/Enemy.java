package be.vdab.lamb.character;

import be.vdab.lamb.items.weapons.Weapon;

import java.io.Serializable;
import java.util.List;

public class Enemy extends Mob implements Serializable {

    private EnemyRace race;
    private EnemyClass enemyClass;
    private int ac;
    private int cr;
    private int hp;
    private List inventory;
    private double xpGain;

    public Enemy(EnemyRace race, EnemyClass enemyClass) {
      cr = enemyClass.cr + race.cr;
      ac = enemyClass.ac + race.ac;
      hp = enemyClass.hp + race.hp;
    }

    public static Enemy createEnemy(){
        Enemy enemy = new Enemy(EnemyRace.GOBLIN, EnemyClass.WARRIOR);
        return enemy;
    }

    public double getXpGain(){
        switch (getCr()){
            case 1: return 100;
            case 2: return 150;
            case 3: return 450;
            case 4: return 800;
            case 5: return 2500;
        }
        return 0;
    }

    public EnemyRace getRace() {
        return race;
    }

    public void setRace(EnemyRace race) {
        this.race = race;
    }

    public EnemyClass getNmeClass() {
        return enemyClass;
    }

    public void setNmeClass(EnemyClass enemyClass) {
        this.enemyClass = enemyClass;
    }

    @Override
    public Weapon getCurrentWeapon() {
        return null;
    }

    @Override
    public int getLvl() {
        return 0;
    }

    @Override
    public int getStrMod() {
        return 0;
    }

    public int getAc() {
        return ac;
    }

    public void setAc(int ac) {
        this.ac = ac;
    }

    public int getCr() {
        return cr;
    }

    @Override
    public void setXp( double xp) {

    }

    public void setCr(int cr) {
        this.cr = cr;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    @Override
    public int getDexMod() {
        return 0;
    }

    @Override
    public int getWisMod() {
        return 0;
    }

    @Override
    public double getXp() {
        return 0;
    }

    public List getInventory() {
        return inventory;
    }

    public void setInventory(List inventory) {
        this.inventory = inventory;
    }
}


package be.vdab.lamb.character;

public enum PlayerClass {
    FIGHTER(18,11,14,8,13,15,12,8),
    RANGER(8,18,13,14,11,15,8,6),
    HEALER(8,11,18,13,15,14,6,4);


    int str;
    int dex;
    int wis;
    int intell;
    int charisma;
    int constit;
    int lifedice;
    int classBaseArmor;




    PlayerClass(int str, int dex , int wis , int intell , int charisma, int constit, int lifedice, int classBaseArmor) {
    this.str = str;
    this.dex = dex;
    this.wis = wis;
    this.intell = intell;
    this.charisma = charisma;
    this.constit = constit;
    this.lifedice = lifedice;
    this.classBaseArmor = classBaseArmor;

    }
}

package be.vdab.lamb.character;

public enum EnemyRace {
    GOBLIN(1,2,1),
    HUMAN(2, 4, 4),
    DWARF(2, 6, 2),
    ELF(2, 2, 6),
    BUGBEAR(2, 4, 10),
    TROLL(3, 6, 20);


    int hp;
    int ac;
    int cr;

    EnemyRace(int cr, int ac, int hp) {
        this.cr = cr;
        this.ac = ac;
        this.hp = hp;
    }
}
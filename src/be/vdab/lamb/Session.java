package be.vdab.lamb;

import be.vdab.lamb.character.Mob;
import be.vdab.lamb.character.Player;
import be.vdab.lamb.map.Direction;
import be.vdab.lamb.map.Door;
import be.vdab.lamb.map.Map;
import be.vdab.lamb.map.Room;

import java.io.*;

public final class Session implements Serializable {


    private Map map;
    private int x;
    private int y;
    private boolean running;
    private Player player;

    public Session(Player player, Map map) {
        this.map = map;
        this.x = map.getStartingX();
        this.y = map.getStartingY();
        map.getRoom(x, y).markVisited();
        this.running = true;
        this.player = player;


    }

    public void showMap() {
        map.printMap(x, y);
    }

    public void attemptMovement(Direction dir) {
        Room other = map.getRoom(x, y, dir);
        if (other == null) {
            System.out.println("There is nothing in that direction.");
            return;
        }
        //Check for doors
        Door door = map.getRoom(x, y).getDoor(dir);
        if (door != null) {
            if (!door.isOpened()) {
                door.open(dir.getOpposite());
                if (!door.isOpened()) return;
            }
        }
        this.x = x + dir.getOffsetX();
        this.y = y + dir.getOffsetY();
        other.markVisited();
        System.out.println("You enter the next room...");
        if (other.hasBonfire()) System.out.println("There's a bonfire here where you could rest at.");
        int amountOfEnemies = other.getEnemies().size();
        if (amountOfEnemies > 0) System.out.println("There are "+amountOfEnemies+" enemies in this room!");
    }


    public void showRoutes() {
        map.showPossibleRoutes(x, y);
    }

    /**
     * Special behaviour:
     * this method will 'hijack' the current thread and keep cycling within itself.
     * It only returns when the user manually closes the loop or if the program is shut down.
     */
    public void start() {
        System.out.println();
        System.out.println("Welcome to " + map.getMapName() + ", " + player.getName()+".");
        System.out.println("Type 'help' for a list of commands.");
        while (running) {
            Commands.processInput(this);
        }
    }

    public void save(boolean forcedSave) {
        if (!forcedSave) {
            if (map.getRoom(x, y).hasBonfire()) {
                System.out.println("You sit down to rest at the bonfire.. Progress saved.");
            } else {
                System.out.println("You can only save your progress in rooms with a bonfire.");
                return;
            }
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(".\\saves\\" + player.getName() + ".ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(this);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }


    public void close() {
        this.running = false;
    }

    public void rest(){
       player.setSpeed(player.getSpeed());
    }

    public void attack(Player attacker,  Mob defender, boolean hasInitiative){
        Dice initDice = new Dice(2);
        int result = initDice.roll(2);
        if(result == 1){

        }
        if(defender.getHp() <= 0){
            StringBuilder sb = new StringBuilder();
            sb.append("you killed the enemy and gained: ").append(defender.getXpGain()).append(" xp!").toString();
            System.out.println(sb);
            attacker.setXp(attacker.getXp() + defender.getXpGain());
        }
        if(attacker.getHp() <= 0){
            StringBuilder deathmsg = new StringBuilder();
            deathmsg.append("You are dead, better luck next time. Returning to last Save point...").toString();
            System.out.println(deathmsg);
        }





    }

}
